package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void multiplesOfSevenUpTo(int n) {
        int x = 7;
        while(x<= n) {
            System.out.println(x);
            x += 7;
        }
        
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i+":");
            for (int x = 1; x<= n; x++) {
                System.out.print(" "+x*i+"");
            
            }
            System.out.println("");
        }
        
        }
    

    public static int crossSum(int num) {
        int sum = 0;
        while (num > 0) {
            sum = sum + num %10;
            num = num /10;
        }
        return sum;
    }

}