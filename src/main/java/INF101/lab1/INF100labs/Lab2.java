package INF101.lab1.INF100labs;

import java.util.logging.Handler;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
       System.out.println(isEvenPositiveInt(123));
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int len1 = word1.length();
        int len2 = word2.length();
        int len3 = word3.length();
        if (len1 >= len2 && len1 >= len3) {
            System.out.println(word1);
        } 
        if(len2 >= len1 && len2 >= len3) {
            System.out.println(word2);
        } 
        if(len3 >= len2 && len3 >= len1) {
            System.out.println(word3);
        }

    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
            return true;
        } else if (year % 4 == 0 && year % 100 == 0) {
            return false;
        } else if (year % 4 == 0) {
            return true;
        } else {
            return false;
        }  
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        } else {
            return false;
        }
        
    }

}
