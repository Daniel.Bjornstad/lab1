package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> a = new ArrayList<>();
        for (int i : list) {
            a.add(i*2);
        }
            return a;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> b = new ArrayList<>();
        for (int i: list) {
            if (i != 3) {
                b.add(i);
            }

        }

        return b;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> c = new ArrayList<>();
        for (int i : list) {
            if (c.contains(i)) {
                continue;
            } else c.add(i);
        }
        return c;

    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i<= a.size() -1; i+=1) {
            a.set(i, b.get(i) + a.get(i));
        }
        

    }

}