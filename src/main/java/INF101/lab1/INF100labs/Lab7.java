package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        
        if (grid.isEmpty()) {
            return false;
        }
        int expectedSum = getRowSum(grid, 0);

        for (int i = 0; i < grid.size(); i++) {
            int rowSum = 0;
            int colSum = 0;
            
            for (int j = 0; j <grid.size(); j++){
                rowSum += grid.get(i).get(j);
                colSum += grid.get(j).get(i);
            }
            if (rowSum != expectedSum || colSum != expectedSum) {
                return false;
            }
                
        }
        
        return true;
    }
    private static int getRowSum(ArrayList<ArrayList<Integer>> grid, int rowIndex) {
        int sum = 0;
        for (int num : grid.get(rowIndex)) {
            sum += num;
        }
        return sum;
    } 

    }

